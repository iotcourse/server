// Import packages
var express = require('express');
var bodyParser = require('body-parser');
var { Client } = require('pg');

// Import configurations from config.js module
const config = require("./config");

// Create an object from express package to start a server
var app = express();

// Some configurations to read request parameters
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Create a database client
const client = new Client(config.pgOptions);

// Establish a connection with postgreSQL db
client.connect();

// Create a GET request to send data to front-end (client/browser)
app.get('/getdata', (req, res) => {
    client.query("SELECT time,"+req.query.col +" FROM data LIMIT 1000",[],(err,result)=>{
        if(err){// en case of error
            console.log("ERROR");
            res.send({ err: "There is a problem" });
        }else{// en case of success
            console.log("SUCCESS");
            res.send({ data:result.rows });
        }
    });
});

// Start the server specifying the port to use
app.listen(config.serverPort, function () {
    console.log('Example app listening on port '+config.serverPort);
});

